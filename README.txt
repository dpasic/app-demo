Note: https://dpasic@bitbucket.org/dpasic/app-demo.git will be used as an example

1)
- create a new remote Git repository
- open Git Bash at the desired location

- clone the repository to your location
git clone https://dpasic@bitbucket.org/dpasic/app-demo.git
cd app-demo

OR

- create your project folder
mkdir app-demo (or use an existing project)
cd app-demo

- initialize the project for Git
git init

- connect your existing repository to the remote
git remote add origin https://dpasic@bitbucket.org/dpasic/app-demo.git
- the '-u' option automatically sets that upstream for you,
linking your repo to a central one; that way, in the future, Git "knows" where you want to push to and
where you want to pull from, so you can use git pull or git push without arguments
git push -u origin master


2)
- add README.txt to the project folder

- stage the changes on README.txt
git add README.txt

- commit the staged changes with the message
git commit -m "Initial commit"

- push the commit to the set origin remote
git push origin master

git add README.txt
git commit -m "README update"
git push origin master

3)
- create a new branch called 'feature-images' and go to it
git checkout -b feature-images

- stage all changes
git add -A

git commit -m "Add images"

- push the commit to feature-images branch
git push origin feature-images

- go back to master branch
git checkout master

- merge the changes from feature-images branch to master branch
git merge feature-images

git push origin master

4)
git add -A
git commit -m "Add additional images"
git push origin master

git add -A
git commit -m "Update additional images"
git push origin master

git add -A
git commit -m "Update additional images"
git push origin master

git add -A
git commit -m "README third and fourth part"
git push origin master